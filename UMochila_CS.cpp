/**********************************************************
** PROGRAMA EM C PARA RESOLVER O PROBLEMA DA MOCHILA POR **
** MEIO DO METODO HIBRIDO CLUSTERING SEARCH              **
**                                                       **
** AUTOR: ANTONIO AUGUSTO CHAVES (ICT/UNIFESP)           **
***********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <omp.h>

#pragma hdrstop
#pragma argsused

#define MAX(x,y) ((x)<(y) ? (y) : (x))
#define INFINITO 99999999


//Dados do problema
#define n 50                 // numero de objetos
#define capacidade 100       // capacidade da mochila

char nomearqB[50] = "beneficio50.txt";
char nomearqP[50] = "peso50.txt";


//Parametros do CS
const int NC = 10;     	     // numero de cluster do CS
const int lambda = 15;       // limitante para tornar um cluster promissor
const int rMax = 10;   	     // numero maximo de busca local sem melhora em um cluster

//Tipo Solução
struct TSol
{
  int obj[n];
  int fo;
};

//Tipo Cluster
struct TCluster
{
    TSol centro;       /***solucao central do cluster (representante das demais solucoes)  ***/
    int v;             /***quantidade de solucoes em determinado cluster				   ***/
    int r;             /***quantidade de vezes em que foi realizada busca local no cluster ***/
};
TCluster cluster[NC];

//melhor solucao encontrada no CS
TSol melhorSolCS;

//Variáveis Globais
int B[n],                   //vetor com os benefícios dos objetos
    P[n];                   //vetor com os pesos dos objetos


//Declaração de Funções
void LerArquivos();
int CalcularFO(TSol s);
TSol GerarSolucaoInicial();
TSol GerarSolucaoInicialViavel();
void ImprimirMelhorSol(TSol s);
TSol TrocarBit(TSol s, int i);
TSol TrocarObjetos(TSol s, int i, int j);
TSol SubidaTrocaBit(TSol s);
TSol SubidaTrocaObjetos(TSol s);
void ILS();
TSol RVND(TSol s);
void CriarClusters();
TSol PR(TSol atual, TSol guia);
void IC(TSol s);
TSol Perturbacao(TSol s);

double randomico(double min, double max);
int irandomico(int min, int max);





/************************************************************************************
*** Método: Main()                                                                ***
*** Função: Programa principal                                                    ***
*************************************************************************************/
int main()
{
  //srand(time(NULL));
  LerArquivos();
  melhorSolCS.fo = -INFINITO;
  
  //criar os clusters iniciais
  CriarClusters();
  
  //Iniciar a execucao da MH geradora de solucoes do CS
  ILS();

  return 0;
}

/************************************************************************************
*** Método: LerArquivos()                                                         ***
*** Função: Ler os arquivos de beneficios e pesos dos objetos                     ***
*************************************************************************************/
void LerArquivos()
{
  int j, valor;
  FILE *arquivo;

  //Ler os benefícios dos objetos
  arquivo = fopen(nomearqB,"r");
  if (!arquivo) {
     printf("O Arquivo %s nao pode ser aberto.\n", nomearqB);
     getchar();
     exit(1);
  }
  j=0;
  while (!feof(arquivo)){
    fscanf(arquivo, "%d", &valor);
    B[j] = valor;
    j++;
  }
  fclose(arquivo);

  //Ler os pesos
  arquivo = fopen(nomearqP,"r");
  if (!arquivo) {
     printf("O Arquivo %s nao pode ser aberto.\n", nomearqP);
     getchar();
     exit(1);
  }
  j=0;
  while (!feof(arquivo)){
    fscanf(arquivo, "%d", &valor);
    P[j] = valor;
    j++;
  }
  fclose(arquivo);
}

/************************************************************************************
*** Método: CalcularFO(TSol s)                                                    ***
*** Função: Calcula o valor da função objetivo da solução s                       ***
*************************************************************************************/
int CalcularFO(TSol s)
{
  int fo,
      beneficio = 0,
      peso = 0,
      inviabilidade = 0,
      penalidade = n*10;

  for (int i=0; i<n; i++)
  {
    if (s.obj[i] == 1)
    {
      beneficio += B[i];
      peso += P[i];
    }
  }

  inviabilidade = MAX(0, peso - capacidade);
  fo = beneficio - (penalidade * inviabilidade);

  return fo;
}

/************************************************************************************
*** Método: TrocarBit(TSol s, int i)                                              ***
*** Função: Troca um bit da solução s                                             ***
*************************************************************************************/
TSol TrocarBit(TSol s, int i)
{
  if (s.obj[i] == 1)
    s.obj[i] = 0;
  else
    s.obj[i] = 1;

  return s;
}

/************************************************************************************
*** Método: TrocarObjetos(TSol s, int i, int j)                                   ***
*** Função: Troca os bits dos objetos i e j da solução s                          ***
*************************************************************************************/
TSol TrocarObjetos(TSol s, int i, int j)
{
  int temp = s.obj[i];
  s.obj[i] = s.obj[j];
  s.obj[j] = temp;

  return s;
}


/************************************************************************************
*** Método: GerarSolucaoInicialViavel()                                                 ***
*** Função: Gerar Solucao Inicial Viavel Aleatorimante                                   ***
*************************************************************************************/
TSol GerarSolucaoInicialViavel()
{
  TSol s;
  int ins, //indicador da inserção (1) ou não (0) de um objeto
      peso = 0; //para verificar restrição de capacidade

	//inicializacao da solucao
	for (int i=0; i<n; i++)
	   s.obj[i] = 0;

    for (int j=0; j<n; j++)
	{
       ins = irandomico(0,1);
       if (ins == 1)
       {
           peso+= P[j];
           if (peso <= capacidade)
              s.obj[j] = 1;
           else
           {
              peso -= P[j];
              break;
           }
       }
    }
    // printf("Solucao inicial viavel: peso = %d\n", peso);
    s.fo = CalcularFO(s);
    return s;
}


/************************************************************************************
*** Método: GerarSolucaoInicial()                                                 ***
*** Função: Gerar Solucao Inicial Aleatorimante                                   ***
*************************************************************************************/
TSol GerarSolucaoInicial()
{
  TSol s;
  for (int j=0; j<n; j++)
    s.obj[j] = irandomico(0,1);

  s.fo = CalcularFO(s);

  return s;
}


/*****************************************************************************
*** Método: ITERATED LOCAL SEARCH                                          ***
*** Funcao: ILS implementado para resolver o problema da mochila           ***
******************************************************************************/
void ILS()
{
  double beta = 0;                            //taxa de perturbação
  int Iter = 0;
  int IterMax = 3000;
  int IterMelhora = 0;

  TSol s,
       sMelhor,
       sViz,
       sMelhorViz;

  //Criar a solucao inicial do ILS
  s = GerarSolucaoInicialViavel();

  //aplicar busca local
  s = SubidaTrocaBit(s);

  //atualizar a melhor solucao encontrada pelo ILS
  sMelhor = s;

  while (Iter < IterMax)
  {
    Iter++;

	//s' <- perturbar a melhor solucao s*
    beta = randomico(0.15,0.25);

    //perturbar a solucao corrente (s)
    sViz = s;
    double pert = n * beta;     
    for (int i=0; i<pert; i++)
    {
      int pos = irandomico(0,n-1);
      sViz = TrocarBit(sViz,pos);
    }
    sViz.fo = CalcularFO(sViz);

    //s*' <- busca local (s')
    sMelhorViz = SubidaTrocaBit(sViz);

    //Enviar a solucao corrente ao componente IC do CS
    IC(sMelhorViz);

    //s* <- criterio de aceitação (s*,s*', historico)
    if (sMelhorViz.fo > s.fo)
    {
      s = sMelhorViz;
      IterMelhora = Iter;

      //verificar se eh a melhor solucao
      if (s.fo > sMelhor.fo)
         sMelhor = s;
    }

    //aplicar history
    if (Iter - IterMelhora > 1000)   //1000 iteracoes sem melhora
    {
      //reiniciar a busca
      s = GerarSolucaoInicialViavel();

      //verificar se eh a melhor solucao
      if (s.fo > sMelhor.fo)
         sMelhor = s;

      IterMelhora = Iter;
    }

    printf("\nIter: %d \t fo: %8d \t melhorILS: %8d \t melhorCS: %8d", Iter, s.fo, sMelhor.fo, melhorSolCS.fo);

  } //fim-while_ILS

  ImprimirMelhorSol(melhorSolCS);
}


/***************************************************************************************
*** Metodo: IC(TSol s)                                                               ***
*** Funcao: aplica o proceso de agrupamento do CS                                    ***
****************************************************************************************/
void IC(TSol s)
{
    int menorDist = INFINITO;
    int distancia = 0;
    int K=0; //cluster mais similar

    /***calcula a distancia da solucao para cada cluster - distancia euclidiana***/
    for(int i=0; i<NC; i++)
    {
        distancia = 0;
        for(int j=0; j<n; j++)
        {
            if (s.obj[j] != cluster[i].centro.obj[j])
               distancia++;
        }

        if (distancia < menorDist)
        {
            menorDist = distancia;
            K = i;
        }
    }

    /***Aplicar o metodo Path Reliking para adicionar a nova solucao ao cluster***/
    //solucao inicial do PR sera a solucao agrupada e a solucao guia o centro do cluster
    cluster[K].centro = PR(s, cluster[K].centro);

    //aumentar o volume
    cluster[K].v++;

    //verificar se é o melhor centro obtido até então
    if(cluster[K].centro.fo > melhorSolCS.fo)
    	melhorSolCS = cluster[K].centro;

    /***se o volume atingiu o limitante lambda***/
    if(cluster[K].v >= lambda)
    {
        cluster[K].v = 0;
        /***e se eh menor que o limitante rMax***/
        if(cluster[K].r < rMax)
        {
            /***aplica-se busca local***/
            TSol newC = RVND(cluster[K].centro);

            /***se o valor de FO do novo centro eh pior que FO do centro do cluster, acrescenta-se o r ***/
            if(newC.fo <= cluster[K].centro.fo)
            {
                cluster[K].r++;
            }
            else
            {
                /***se o valor de FO do novo centro eh melhor que FO do centro do cluster, ele se torna novo centro***/
                cluster[K].centro = newC;
                cluster[K].r = 0;


                /***se o valor de FO da solucao encontrada eh melhor do que o melhor encontrado ate o momento, atualiza-se a solucao final do CS***/
                if(cluster[K].centro.fo > melhorSolCS.fo)
                  melhorSolCS = cluster[K].centro;
            }
        }
        else
        {
            /***se o r atingir seu limitante, uma pertubacao eh feita em uma dada porcentagem de clientes***/
            cluster[K].centro = Perturbacao(cluster[K].centro);
            cluster[K].r = 0;
            cluster[K].v = 0;
        }
    }
}


/***************************************************************************************
*** Metodo: Criarclusters()                                                           ***
*** Funcao: Criar os clusters iniciais                                               ***
****************************************************************************************/
void CriarClusters()
{
	for (int i=0; i<NC; i++)
	{
		cluster[i].centro = GerarSolucaoInicialViavel();
	
		if(cluster[i].centro.fo > melhorSolCS.fo)
			melhorSolCS = cluster[i].centro;
	}
}


/************************************************************************************
*** Método: RVND(TSol s)                                                          ***
*** Função: Aplicar o metaheuristica VND para obter um otimo local                ***
*************************************************************************************/
TSol RVND(TSol s)
{
    TSol sViz;
    int k, it;

    //definir a ordem das vizinhancas aleatoriamente
    int v[2] = {1,2};
    for (int i=0; i<n; i++)
    {
        int p1 = irandomico(0,1);
        int p2 = irandomico(0,1);

        int temp = v[p1];
        v[p1] = v[p2];
        v[p2] = temp;
    }

    //aplicar busca local na solucao corrente s
    k = 0;
    
    while (k < 2)
    {
        it++;
        //copiar solucao corrente
        sViz = s;

        //pesquisar a vizinhanca Nk
        if (v[k] == 1)
          sViz = SubidaTrocaBit(sViz);
        else
        if (v[k] == 2)
          sViz = SubidaTrocaObjetos(sViz);

        //verificar se o melhor vizinho eh melhor que a solucao corrente
        if ( sViz.fo > s.fo )
        {
          //continua a busca a partir da solução vizinha e retorna para primeira vizinhanca
          s = sViz;
          k = 0;
        }
        else
        {
          k++;
        }
    }

    return s;
}

/************************************************************************************
*** Método: SubidaTrocaBit()                                                      ***
*** Função: Heuristica de subida que examina a troca de todos os bits             ***
*************************************************************************************/
TSol SubidaTrocaBit(TSol s)
{
    TSol sLocal = s;       //armazena a melhor solucao vizinha
    TSol sViz;
    int i;

    for (i=0; i<n; i++)
    {
      //trocar um bit
      sViz = TrocarBit(s, i);
      sViz.fo = CalcularFO(sViz);

      //armazenar a melhor troca se a solucao melhorar
      if (sViz.fo > sLocal.fo)
      {
        sLocal = sViz;

        //first
        s = sViz;
      }

    }

    //retorna a melhor solucao
    return sLocal;
}


/************************************************************************************
*** Método: SubidaTrocaObjetos()                                                  ***
*** Função: Heuristica de subida que examina a troca de todos os objetos          ***
*************************************************************************************/
TSol SubidaTrocaObjetos(TSol s)
{
    TSol sLocal = s;       //armazena a melhor solucao vizinha
    TSol sViz;

    for (int i=0; i<n-1; i++)
    {
        for (int j=i+1; j<n; j++)
        {
            //trocar dois objetos na mochila
            if (s.obj[i] != s.obj[j])
            {
                sViz = TrocarObjetos(s, i, j);
                sViz.fo = CalcularFO(sViz);

              	//armazenar a melhor troca se a solucao melhorar
                if (sViz.fo > sLocal.fo)
                {
                  sLocal = sViz;

                  //first
                  s = sViz;
                }
            }
        }
    }
    //retorna a melhor solucao
    return sLocal;
}



/************************************************************************************
*** Metodo: PR()                                                                  ***
*** Funcao: Aplica a metodo Path Relinking                                        ***
*************************************************************************************/
TSol PR(TSol atual, TSol guia)
{
    TSol melhorCaminho = atual;     //melhor solucao obtida pelo PR
    TSol melhorIteracao = atual;    //melhor solucao em cada iteracao
    TSol sCorrente = atual;         //solucao corrente em cada iteracao
    TSol sViz = atual;              //solucao vizinha em cada iteracao

    //calcula a diferença entre as solucoes
    int dist=0;
    int diferenca[n];
    for (int i=0; i<n; i++)
    {
    	if (atual.obj[i] == guia.obj[i])
      	   diferenca[i] = 0;
        else
        {
      	   diferenca[i] = 1;
           dist++;
        }
    }

    //se houver diferença entre as solucoes 'zerar' a melhor solucao do caminho
    if (dist > 0)
        melhorCaminho.fo = -INFINITO;

    //inicializa o processo de busca do PR
    int melhorindice = -1;
    int limiteCaminho = (int)(dist*0.7); //pesquisar somente em 30% do caminho
    while(dist > limiteCaminho)
    {
        melhorindice = -1;
        melhorIteracao.fo = -INFINITO;

        //examinar todas as trocas possíveis em uma iteração do PR
        for(int i=0; i<n; i++)
        {
            if(diferenca[i] == 1)
            {
                //gerar o vizinho da solucao corrente
                sViz = sCorrente;
                sViz.obj[i] = guia.obj[i];
                sViz.fo = CalcularFO(sViz);

                //verficar se é a melhor solucao da iteracao
                if(sViz.fo > melhorIteracao.fo){
                    melhorIteracao = sViz;
                    melhorindice = i;
                }
            }
        }

        //verificar se é a melhor solucao do caminho
        if (melhorIteracao.fo > melhorCaminho.fo)
           melhorCaminho = melhorIteracao;

        //continuar a busca a partir da melhor solucao da iteracao
        sCorrente = melhorIteracao;

        //apagar o indice do cliente trocado nesta iteracao
        diferenca[melhorindice]=0;

        //diminuir o número de movimentos possíveis
        dist--;
    }

    return melhorCaminho;
}


/************************************************************************************
*** Metodo: Pertubacao(Sol s)                                                     ***
*** Funcao: Gera uma solucao vizinha aleatoria                                    ***
************************************************************************************/
TSol Perturbacao(TSol s)
{
    //gerar solucoes aleatorias
  	int p1;
    for(int i=0; i<n*0.3; i++)
    {
    	//escolher uma posicao aleatoria
      p1 = irandomico(0,n-1);

      //gerar vizinho
      s = TrocarBit(s,p1);
    }

    s.fo = CalcularFO(s);
    return s;
}


/************************************************************************************
*** Método: ImprimirMelhorSol()                                                   ***
*** Função: Imprimir a melhor solucao encontrada                                  ***
*************************************************************************************/
void ImprimirMelhorSol(TSol s)
{
  printf("\n\n\nMELHOR SOLUCAO ENCONTRADA");
  printf("\n*************************");
  printf("\n\nFO: %d", s.fo);
  printf("\n\nObjetos selecionados: \n");
  printf("--------------------\n");
  for (int i=0; i<n; i++)
    if (s.obj[i])
      printf(" %d ",i);

  printf("\n\nObjetos nao selecionados: \n");
  printf("------------------------\n");
  for (int i=0; i<n; i++)
    if (!s.obj[i])
      printf(" %d ",i);
	  
  printf("\n\n*************************\n\n");
}


/************************************************************************************
*** Método: randomico(double min, double max)                                     ***
*** Função: Gera um numero aleatorio entre min e max                              ***
*************************************************************************************/
double randomico(double min, double max)
{
	return ((double)(rand()%10000)/10000.0)*(max-min)+min;
}

/************************************************************************************
*** Método: irandomico(int min, int max)                                          ***
*** Função: Gera um numero inteiro aleatorio entre min e max                      ***
*************************************************************************************/
int irandomico(int min, int max)
{
	return (int)randomico(0,max-min+1.0) + min;
}
